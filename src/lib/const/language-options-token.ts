import { LanguageModuleOptions } from '../interface/language-module-options';
import { InjectionToken } from '@angular/core';

export const LANGUAGE_OPTIONS_TOKEN = new InjectionToken<LanguageModuleOptions>('LANGUAGE_OPTIONS_TOKEN');


