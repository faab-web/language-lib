import { Updatable } from '@faab/base-lib';
import { LanguageEnum } from '../enum/language.enum';

export class LocalizedString extends Updatable {

  constructor(lang?: LanguageEnum, value?: string) {
    super();
    this.language = lang;
    this.value = value;
  }

  language: LanguageEnum;
  key: string;
  value: string;

}
