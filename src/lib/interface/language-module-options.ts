import { LanguageEnum } from '../enum/language.enum';
import { Language } from './language';

export interface LanguageModuleOptions {
  languages: Language[];
  default: LanguageEnum;
}
