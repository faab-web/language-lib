import {LanguageEnum} from '../enum/language.enum';

export interface Language {
  code: LanguageEnum;
  title: string;
}
