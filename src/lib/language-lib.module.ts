import { LanguageModuleOptions } from './interface/language-module-options';
import { LANGUAGE_OPTIONS_TOKEN} from './const/language-options-token';
import { HttpClient } from '@angular/common/http';
import { TranslateService, TranslateStore } from '@ngx-translate/core';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { LanguageLibService } from './language-lib.service';

import {NgModule, ModuleWithProviders} from '@angular/core';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '/assets/i18n/', `.json`);
}




@NgModule({
  declarations: [],
  imports: [
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [TranslateModule]
})
export class LanguageLibModule {
  static forRoot(options?: LanguageModuleOptions): ModuleWithProviders {
    return {
      ngModule: LanguageLibModule,
      providers: [
        HttpClient,
        TranslateService,
        TranslateStore,
        {
          provide: LANGUAGE_OPTIONS_TOKEN,
          useValue: options,
        },
        LanguageLibService
      ]
    };
  }
}
