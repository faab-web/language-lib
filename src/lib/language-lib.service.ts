import { TranslateService } from '@ngx-translate/core';
import { Injectable, EventEmitter, Inject } from '@angular/core';
import { LanguageModuleOptions } from './interface/language-module-options';
import { LANGUAGE_OPTIONS_TOKEN } from './const/language-options-token';
import { Language } from './interface/language';
import { LanguageEnum } from './enum/language.enum';
import { LocalizedString } from './entity/localized-string';

@Injectable({
  providedIn: 'root'
})
export class LanguageLibService {

  // field: string = 'lang';

  languages: Language[];
  private current_code: string;
  default: LanguageEnum = LanguageEnum.RU;
  change: EventEmitter<string> = new EventEmitter<string>();

  listenerPath: any;
  config_set: LanguageModuleOptions;

  constructor(
    @Inject(LANGUAGE_OPTIONS_TOKEN) private config: LanguageModuleOptions,
    private translate: TranslateService
  ) {
    this.config_set = config;
    this.init();
  }

  init() {
    if (this.config_set) {
      this.default = (typeof this.config_set.default === 'string'
      && typeof LanguageEnum[this.config_set.default.toUpperCase()] === 'string')
      ? this.config_set.default : LanguageEnum.RU;
      this.languages = this.config_set.languages ? this.config_set.languages : [{
        code: LanguageEnum.RU,
        title: 'Русский'
      }as Language];
      this.translate.use(this.default);
    }
  }

  getCurrentLanguage(): string {
    // this.config;
    if (!this.current_code) {
      this.setLanguage(this.default);
    }
    return this.current_code;
  }

  getLanguages(): Language[] {
    return this.languages;
  }

  setLanguage(code: string): void {
    if (!this.isAllow(code)) {
      code = this.default;
    }
    code = LanguageEnum[code.toUpperCase()];
    if (!this.current_code || this.current_code !== code) {

      this.current_code = code;
      this.translate.use(code);
    }

    this.change.emit(this.current_code);
  }

  isAllow(code: string): boolean {
    try {
      code = LanguageEnum[code.toUpperCase()];
      if (code) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  getLocalizedString(date: LocalizedString[] | string | number): string {
    let res = '';
    if (typeof date === 'string' || typeof date === 'number') {
      return date.toString();
    }
    if (!Array.isArray(date)) {
      return res;
    }
    date.forEach(value => {
      if (value.language === this.getCurrentLanguage()) {
        res = value.value;
      }
    });
    return res;
  }

  createLocalizedStrings(): LocalizedString[] {
    const localStrings = [];
    this.languages.forEach(lang => {
      localStrings.push(new LocalizedString(lang.code));
    });
    return localStrings;
  }

  instant(key: string | Array<string>, interpolateParams?: Object): string {
    const i18n: string | any = this.translate.instant(key, interpolateParams);
    return typeof i18n === 'string' ? i18n : '';
  }

}
